'use strict';
import React from "react";
import ReactDOM from "react-dom";

class Layout extends React.Component {

	render() {
		return(
			<h1>Hello to ReactJS!</h1>
		);
	}
};

const example = document.getElementById('example');
ReactDOM.render(<Layout/>, example);